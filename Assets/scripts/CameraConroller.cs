using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target1; // Reference to the first player's transform
    public Transform target2; // Reference to the second player's transform
    public float smoothSpeed = 0.5f; // Controls the smoothness of the camera movement
    public Vector3 offset; // The offset of the camera from the players

    private Vector3 desiredPosition; // The position the camera wants to move towards

    private void LateUpdate()
    {
        // Calculate the midpoint between the two players
        Vector3 midpoint = (target1.position + target2.position) * 0.5f;

        // Calculate the desired position of the camera
        desiredPosition = midpoint + offset;

        // Smoothly move the camera towards the desired position
        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
    }
}