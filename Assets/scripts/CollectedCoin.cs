using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectedCoin : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("rope"))
        {
            PlayerManager.numberOfCoins += 1;
            Destroy(gameObject);
            

        }
    }
}
