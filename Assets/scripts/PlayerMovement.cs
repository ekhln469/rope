using UnityEngine;
using Obi;

public class PlayerController : MonoBehaviour
{
    private CharacterController controller;

    public float movementSpeed = 5f;
   

    private Vector3 initialMousePosition;
    private bool isSwiping = false;

    public GameObject CompletedPanel;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (!PlayerManager.isGameStarted)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            isSwiping = true;
            initialMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isSwiping = false;
        }

        if (isSwiping)
        {
            Vector3 currentMousePosition = Input.mousePosition;
            float swipeDistance = currentMousePosition.x - initialMousePosition.x;

            if (swipeDistance > 0)
            {
                // Swipe right
                MovePlayers(Vector3.right);
            }
            else if (swipeDistance < 0)
            {
                // Swipe left
                MovePlayers(Vector3.left);
            }
        }
        controller.center = controller.center;

        // Move both players forward
        MovePlayers(Vector3.forward);
    }

    private void MovePlayers(Vector3 direction)
    {
        // Move both players in the specified direction
        transform.Translate(direction * movementSpeed * Time.deltaTime);
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("obstacle"))
        {
            PlayerManager.gameOver = true;
        }
        

        if (other.gameObject.CompareTag("complete"))
        {
            CompletedPanel.SetActive(true);
            gameObject.SetActive(false);
        }


    }
}
